# RCL Website
This repo server to contain the files for the Hugo generated, static web site for River City Labs Makerspace

## How to upgrade the theme version
1. copy the [meghna-hugo](https://github.com/themefisher/meghna-hugo) theme from the linked repo into the rcl-site/themes folder
2. Check all files under rcl-site/layouts/partials in their respective theme location (rcl-site//themes/meghna-hugo/layouts/partials/)
) for updates. If there are updates, copy the new file to rcl-site/layouts/partials and add the custom RCL code back in.
3. build and serve the app locally to make sure everything looks and works the same
4. Submit a Pull Request against the main branch
